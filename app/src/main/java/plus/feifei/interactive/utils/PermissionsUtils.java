package plus.feifei.interactive.utils;

import android.app.Activity;
import android.widget.Toast;

import com.hjq.permissions.OnPermissionCallback;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;

import java.util.List;

import plus.feifei.interactive.MainActivity;

public class PermissionsUtils {
    public static void init(Activity activity){
        XXPermissions.with(activity)
                // 申请单个权限
                .permission(Permission.CAMERA)
                // 申请多个权限
                .permission(Permission.Group.BLUETOOTH,Permission.Group.STORAGE)
                // 申请安装包权限
                //.permission(Permission.REQUEST_INSTALL_PACKAGES)
                // 申请悬浮窗权限
                //.permission(Permission.SYSTEM_ALERT_WINDOW)
                // 申请通知栏权限
                //.permission(Permission.NOTIFICATION_SERVICE)
                // 申请系统设置权限
                //.permission(Permission.WRITE_SETTINGS)
                // 设置权限请求拦截器
                //.interceptor(new PermissionInterceptor())
                // 设置不触发错误检测机制
                //.unchecked()
                .request(new OnPermissionCallback() {

                    @Override
                    public void onGranted(List<String> permissions, boolean all) {
                        if (all) {
                            Toast.makeText(activity,"获取权限成功",Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(activity,"获取部分权限成功，但部分权限未正常授予",Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onDenied(List<String> permissions, boolean never) {
                        if (never) {
                            Toast.makeText(activity,"被永久拒绝授权，请手动授予录音和日历权限",Toast.LENGTH_SHORT).show();
                            // 如果是被永久拒绝就跳转到应用权限系统设置页面
                            XXPermissions.startPermissionActivity(activity, permissions);
                        } else {
                            Toast.makeText(activity,"获取权限失败",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}
