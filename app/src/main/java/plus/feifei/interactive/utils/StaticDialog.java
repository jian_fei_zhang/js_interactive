package plus.feifei.interactive.utils;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

public class StaticDialog {
    public static void dialogInfo(Context context, String msg){
        AlertDialog.Builder b=new AlertDialog.Builder(context);
        b.setTitle("提示");
        b.setMessage(msg);
        b.setCancelable(false);
        b.setPositiveButton("知道了", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        b.show();
    }
}
