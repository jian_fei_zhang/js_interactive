package plus.feifei.interactive.utils;

import java.util.Objects;

public class StringUtils {
    public static boolean isBlank(Object obj) {
        if(obj==null){
            return true;
        }
        String str=obj.toString();
        int strLen;
        if (str == null || (strLen = str.length()) == 0) {
            return true;
        }
        for (int i = 0; i < strLen; i++) {
            if ((!Character.isWhitespace(str.charAt(i)))) {
                return false;
            }
        }
        return true;
    }
    public static boolean isNotBlank(Object str) {
        return !isBlank(str);
    }
}
