package plus.feifei.interactive.service;

import android.util.Log;
import android.webkit.JavascriptInterface;

import com.alibaba.fastjson.JSONArray;

import java.util.List;

import plus.feifei.interactive.dao.SqlDao;
import plus.feifei.interactive.utils.StaticDialog;

public class SqlService {
    private SqlDao sqlDao;

    public SqlService(SqlDao sqlDao) {
        this.sqlDao = sqlDao;
    }

    /**
     * 执行增删改
     * @param sql 执行增删改的sql语句，包括创建表的语句
     * @return 执行结果  ok：表示执行成功    非ok：返回错误信息
     */
    @JavascriptInterface
    public String exeSql(String sql){
        try {
            sqlDao.exeSQL(sql);
            return "ok";
        } catch (Exception e) {
            e.printStackTrace();
//            StaticDialog.dialogInfo(sqlDao.getContext(),e.getMessage());
            return e.getMessage();
        }
    }

    /**
     * js调用的方法
     * @param sql 要执行的sql语句，仅限查询
     * @return json格式的数组
     */
    @JavascriptInterface
    public String select(String sql){
        JSONArray list = sqlDao.select(sql);
        for (Object s : list) {
            Log.d("test",s.toString());
        }
        return list.toJSONString();
    }
}
