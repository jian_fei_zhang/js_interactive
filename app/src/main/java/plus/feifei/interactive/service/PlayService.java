package plus.feifei.interactive.service;

import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import plus.feifei.interactive.utils.MediaUtil;

/**
 * 媒体播放
 */
public class PlayService {
    private Context context;

    public PlayService(Context context) {
        this.context = context;
    }
    @JavascriptInterface
    public void play(String fileName){
        MediaUtil.play(fileName,context);
    }
}
