package plus.feifei.interactive.service;

import android.location.Location;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import plus.feifei.interactive.MainActivity;
import plus.feifei.interactive.utils.LocationUtil;

public class LocationService {
    private MainActivity context;

    public LocationService(MainActivity context) {
        this.context = context;
    }

    @JavascriptInterface
    public String getLocation(){
        LocationUtil.getCurrentLocation(context, new LocationUtil.LocationCallBack() {
            @Override
            public void onSuccess(Location location) {
                Toast.makeText(context,location.getLatitude()+"",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFail(String msg) {
                Toast.makeText(context,msg,Toast.LENGTH_SHORT).show();
            }
        });
        return "";
    }
}
