package plus.feifei.interactive.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class ToolsService {
    private Context context;

    public ToolsService(Context context) {
        this.context = context;
    }

    /**
     * 获取当前网络环境
     *
     * @return 流量:flow   ,wifi:wifi ,无网络:none
     */
    @JavascriptInterface
    public String netType() {
        ConnectivityManager conMann = (ConnectivityManager)
                context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mobileNetworkInfo = conMann.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        NetworkInfo wifiNetworkInfo = conMann.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        if (mobileNetworkInfo.isConnected()) {
            return "flow";
        } else if (wifiNetworkInfo.isConnected()) {
            return "wifi";
        } else {
            return "none";
        }
    }
}
