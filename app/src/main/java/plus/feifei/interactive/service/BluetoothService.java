package plus.feifei.interactive.service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import plus.feifei.interactive.MainActivity;
import zpSDK.zpSDK.zpBluetoothPrinter;

public class BluetoothService {
    private zpBluetoothPrinter zpSDK = null;
    private MainActivity context;

    public BluetoothService(MainActivity mainActivity) {
        this.context = mainActivity;
        zpSDK = new zpBluetoothPrinter(mainActivity);
    }

    /**
     * 获取蓝牙列表
     * 返回格式:   [{"address":"74:45:2D:3C:C3:4E","deviceName":"nova 8 Pro"},{"address":"F2:45:4F:4D:F7:1C","deviceName":"Mi Band 3"}]
     */
    @JavascriptInterface
    public String blueList() {
        BluetoothAdapter defaultAdapter = BluetoothAdapter.getDefaultAdapter();
        if (defaultAdapter == null) {
            Toast.makeText(context, "没有找到蓝牙适配器", Toast.LENGTH_LONG).show();
            return "";
        }
        if (!defaultAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            context.startActivityForResult(enableBtIntent, 2);
            return "";
        }
        Set<BluetoothDevice> pairedDevices = defaultAdapter.getBondedDevices();
        List<Map<String, String>> list = new ArrayList<>();
        for (BluetoothDevice device : pairedDevices) {
            Map<String, String> map = new HashMap<String, String>();
            map.put("deviceName", device.getName());
            map.put("address", device.getAddress());
            list.add(map);
        }
        String json = JSONObject.toJSONString(list);
        Log.d("json", json);
        return json;
    }

    /**
     * 连接打印机
     *
     * @param address 蓝牙地址
     */
    @JavascriptInterface
    public void connect(String address) {
        zpSDK.connect(address);
    }

    /**
     * 获取打印机状态
     *
     * @return
     */
    @JavascriptInterface
    public String printInfo() {
        return zpSDK.printerStatus();
    }

    /**
     * 打印文字
     *
     * @param x         起始横坐标
     * @param y         起始纵坐标
     * @param width     文本框宽度
     * @param height    文本框高度
     * @param str       字符串
     * @param fontsize  字体大小 1：16点阵；2：24点阵；3：32点阵；4：24点阵放大一倍；5：32点阵放大一倍
     *                  6：24点阵放大两倍；7：32点阵放大两倍；其他：24点阵
     * @param rotate    旋转角度 0：不旋转；1：90度；2：180°；3:270°
     * @param bold      是否粗体 0：取消；1：设置
     * @param underline 是有有下划线 false:没有；true：有
     * @param reverse   是否反白 false：不反白；true：反白
     */
    @JavascriptInterface
    public void print(int text_x, int text_y, String text, int fontSize, int rotate, int bold, boolean reverse, boolean underline) {
        zpSDK.drawText(text_x,text_y,text,fontSize,rotate,bold,reverse,underline);
    }
}
