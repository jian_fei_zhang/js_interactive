package plus.feifei.interactive.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;


import java.util.Objects;

import plus.feifei.interactive.MainActivity;
import plus.feifei.interactive.R;
import plus.feifei.interactive.config.UserConfig;
import plus.feifei.interactive.utils.StringUtils;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText edtUserName;
    private EditText edtPswd;
    private Button loginBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("请先登录");
        setContentView(R.layout.activity_login);
        init();

    }

    @SuppressLint("WrongViewCast")
    private void init() {
        edtUserName=findViewById(R.id.edt_user_name);
        edtPswd=findViewById(R.id.edt_pswd);
        loginBtn=findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.loginBtn:
                login();
                break;
        }
    }

    private void login() {
        if(StringUtils.isBlank(edtUserName.getText())||StringUtils.isBlank(edtPswd.getText())){
            Toast.makeText(this,"用户名或密码不能为空",Toast.LENGTH_SHORT).show();
            return;
        }
        if(!Objects.equals(edtUserName.getText().toString(), UserConfig.userName)||!Objects.equals(edtPswd.getText().toString(),UserConfig.pswd)){
            Toast.makeText(this,"用户名或密码错误",Toast.LENGTH_SHORT).show();
            return;
        }
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}