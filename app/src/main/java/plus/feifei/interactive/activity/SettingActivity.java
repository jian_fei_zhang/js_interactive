package plus.feifei.interactive.activity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import plus.feifei.interactive.R;
import plus.feifei.interactive.utils.Constants;
import plus.feifei.interactive.utils.SPUtils;

public class SettingActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("设置");
        setContentView(R.layout.setting);
        EditText editText=findViewById(R.id.web_url_id);
        Object o = SPUtils.get(this, Constants.WEB_URL_KEY, "file:///android_asset/index.html");
        editText.setText(o.toString());

        Button save=findViewById(R.id.setting_save);
        save.setOnClickListener(v->{
            SPUtils.put(SettingActivity.this,Constants.WEB_URL_KEY,editText.getText().toString());
        });
    }
}
